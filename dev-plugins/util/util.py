
helpHeader = """
=====================================
SDETool3
=====================================
Help Text
"""

class Plugin():
	"""
	Util.Plugin is to handle dispatching commands from the SDETool3 host.
	It also contains basic command(s) like `help`
	"""
	def __init__(self, pluginLoader):
		self.pluginLoader = pluginLoader
	def handleEvent(self, event):
		if event.Type == "onLoad":
			self.pluginLoader.RegisterEvent("cmd")
			self.pluginLoader.RegisterCommandLineEvent("help", "cmd_util_help", "Prints the text you're reading right now.")
		elif event.Type == "cmd":
			command = event.Args[0]
			try:
				v = self.pluginLoader._commands[command]
				ename = v[0]
				self.pluginLoader.fireEvent(ename, event.Args[1:])
			except KeyError:
				print("No such command " + command)
		elif event.Type == "cmd_util_help":
			print helpHeader
			for cmd in self.pluginLoader._commands:
				print(cmd + ":\n")
				print("\t" + self.pluginLoader._commands[cmd][1])
