import os
import sys
import sqlite3
import cPickle as pickle


class SDEType():
	_parent = None
	Attributes = {}
	TypeID = None
	TypeName = None


	def __init__(self, typeID, typeName, parent):
		self.TypeID = typeID
		self.TypeName = typeName
		self._parent = parent
	def GetAttribute(self, attr):
		return Attributes[attr][1]
	def _AddAttribute(self, Attribute):
		self.Attributes[Attribute[0]] = Attribute
		self.__dict__["Attributes"] = self.Attributes

class SDE():
	_TypeIDs = []
	_Types = []
	_conn = None
	
	# Stuff you should actually use
	Version = ""
	Origin = ""
	Types = {}

	def __init__(self, filename):
		self.Origin = filename
		print("SDE().__init__() from database")
		print("  => Connecting to database")
		self._conn = sqlite3.connect(filename)
		print("  => Loading types table")
		for row in self._conn.execute("SELECT * FROM CatmaTypes;"):
			self._Types.append(row)
			self._TypeIDs.append(row[0])
		print("  => Loading attributes table")
		print("  => Relating entries to objects")
		for outset in self._Types:
			typeID = outset[0]
			typeName = outset[1]
			sdeType = SDEType(typeID, typeName, self)
			print("    => Working on " + str(typeID))
			for attr in self._conn.execute("SELECT * FROM CatmaAttributes WHERE TypeID =:Id;", {"Id":typeID}):
				v = None
				if attr[2] != "None":
					v = attr[2]
				elif attr[3] != "None":
					v = attr[3]
				elif attr[4] != "None":
					v = attr[4]
				sdeType._AddAttribute((attr[1], v))
			self._addType(sdeType)
		self.DumpToSDE(filename)
	def GetTypeByID(self, id):
		return self.Types[id]
	def _addType(self, sdetype):
		try:
			print("Adding:  " + sdetype.Attributes["mDisplayName"][1])
		except:
			print("Adding:  " + str(sdetype.TypeID))
		self.Types[sdetype.TypeID] = sdetype
	def DumpToSDE(self, filename):
		self._conn = None
		self.__dict__["Types"] = self.Types
		self.__dict__["_Types"] = self._Types
		self.__dict__["_TypeIDs"] = self._TypeIDs
		pickle.dump(self, open(filename + ".sde", "wb"))

def LoadSDEFromFile(filename):
	with open(filename, "rb") as f:
		return pickle.load(f)