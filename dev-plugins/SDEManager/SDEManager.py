from sys import path
path = []
path.append("python2.7.zip/usr/lib64/python2.7/")
import os
path.append(os.getcwd() + "/")
import os.path
import urllib
import zipfile
from SDELib import *

class Plugin():
	def __init__(self, pluginLoader):
		self.pluginLoader = pluginLoader
		self.SDEs = {}
		self.SDE = None
		self.VersionDir = os.environ["HOME"] + "/.SDETool/versions/"
		self.Versions = [
			{
				"url": "http://cdn1.eveonline.com/community/DUST_SDE/Warlords_1.0_857519.zip", 
				"zip": True,
				"name": "Warlords_1",
				"latest": True
			},
			{
				"url": "http://cdn1.eveonline.com/community/DUST_SDE/Uprising_1.9_853193.zip", 
				"zip": True,
				"name": "Uprising_1.9",
				"latest": False
			},
			{
				"url": "http://cdn1.eveonline.com/community/DUST_SDE/Uprising_1.8_851720.zip", 
				"zip": True,
				"name": "Uprising_1.8_delta",
				"latest": False
			}
		]
	def handleEvent(self, event):
		if event.Type == "onLoad":
			self.pluginLoader.RegisterCommandLineEvent("loadsde","cmd_SDEManager_LoadSDEFile", "Loads a matching SDE version.")
			self.pluginLoader.RegisterCommandLineEvent("listsde", "cmd_SDEManager_ListSDEVersions", "Lists all available SDE versions.")
			print("SDEManager: Pre-loading newest SDE")
			for v in self.Versions:
				if v["latest"]:
					print("Found " + v["name"])
					self._loadSDEFile(v["name"])

		elif event.Type == "cmd_SDEManager_LoadSDEFile":
			if len(event.Args) == 0:
				print("cmd_SDEManager_LoadSDEFile event fired with no arguments.")
				return
			versionName = event.Args[0]
			print("Attempting to load " + versionName)
			self._loadSDEFile(versionName)
		elif event.Type == "cmd_SDEManager_ListSDEVersions":
			for v in self.Versions:
				print(v["name"])
	def _loadSDEFile(self, versionName):
		og = os.getcwd()
		os.chdir(self.VersionDir)
		try:
			os.makedirs(self.VersionDir)
		except:
			pass
		if not os.path.exists(versionName + ".db"):
			v = None
			for k in self.Versions:
				if k["name"] == versionName:
					v = k
			if v == None:
				print("Version does not exist.")
				return
			print("Downloading file")
			urllib.urlretrieve(v["url"], versionName +".tmp.zip")
			with zipfile.ZipFile(versionName + ".tmp.zip", "r") as z:
				z.extract("dustSDE.db")
				os.rename("dustSDE.db", versionName + ".db")
		if os.path.exists(versionName + ".db.sde"):
			self.SDEs[versionName] = LoadSDEFromFile(versionName + ".db.sde")
		else:
			self.SDEs[versionName] = SDE(versionName + ".db")
		self.SDE = self.SDEs[versionName]
		self.pluginLoader.SDE = self.SDE
		os.chdir(og)
