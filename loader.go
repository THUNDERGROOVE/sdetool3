package main

import (
	"archive/zip"
	"encoding/json"
	"github.com/sbinet/go-python"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

type PluginInfo struct {
	Name            string `json:"name"`
	Version         string `json:"version"`
	Main            string `json:"main"`
	Author          string `json:"author"`
	Core            bool   `json:"core"`
	RequiredVersion string `json:"required-version"`
	Filename        string `json:"-"`
}

var Plugins []PluginInfo

func LoadPlugins() {
	Plugins = make([]PluginInfo, 0)

	if _, err := os.Stat("plugins"); os.IsNotExist(err) {
		Log.Println("Doing initial setup!")
		InitialSetup()
	}

	Log.Println("Loading plugins..")
	if files, err := ioutil.ReadDir("plugins"); err == nil {
		for _, v := range files {
			if !v.IsDir() && strings.Contains(v.Name(), ".zip") {
				//Log.Println("Looking at", v.Name())
				if z, err := zip.OpenReader("plugins/" + v.Name()); err == nil {
					for _, file := range z.File {
						if file.Name == "plugin.json" {
							//Log.Println("Found plugin.json")
							if f, err := file.Open(); err == nil {
								info := PluginInfo{}
								data, _ := ioutil.ReadAll(f)
								json.Unmarshal(data, &info)
								info.Filename = v.Name()
								if info.Main == "" {
									Log.Println("Plugin", v.Name(), "found with no main; omiting")
									continue
								}
								var dupe bool
								if verCompare(info.RequiredVersion, Version) && info.RequiredVersion != "*" {
									Log.Printf("Plugin %v found that requires SDETool3 verison be at least %v but it's %v", info.Name, info.RequiredVersion, Version)
									continue
								} else {
									Log.Printf("%v > %v", Version, info.RequiredVersion)
								}
								for k, v := range Plugins {
									if v.Name == info.Name {
										dupe = true
										Log.Println("Found duplicate plugin.  Attempting to load only the newst version.")
										if verCompare(info.Version, v.Version) {
											Log.Printf("%v > %v", info.Version, v.Version)
											Plugins[k] = info
										}
									}
								}
								if !dupe {
									//updates.CheckForUpdates(info)
									Plugins = append(Plugins, info)
								}
							} else {
								Log.Println("  Error reading plugin.json")
							}
						}
					}
				} else {
					Log.Println("  Error reading zip", err.Error())
				}
			}
		}
	} else {
		Log.Println("Error reading plugins directory", err.Error())
	}
	cores := 0
	for _, v := range Plugins {
		if v.Core {
			cores += 1
		}
	}
	//Log.Printf("Collected %v plugins\nTotal of %v core plugins", len(Plugins), cores)

	Log.Println("Calling SetupPaths")
	python.PyRun_SimpleString("pluginLoader.SetupPaths()")
	//Log.Println("Activating core plugins first")
	for _, v := range Plugins {
		if v.Core {
			Log.Printf("Activating '%v' version %v by '%v'", v.Name, v.Version, v.Author)
			python.PyRun_SimpleString("pluginLoader.LoadPlugin('" + v.Main + "', '" + v.Filename + "')")
		}
	}
	//Log.Println("Activating non-core plugins now")
	for _, v := range Plugins {
		if !v.Core {
			Log.Printf("Activating '%v' version %v by '%v'", v.Name, v.Version, v.Author)
			python.PyRun_SimpleString("pluginLoader.LoadPlugin('" + v.Main + "', '" + v.Filename + "')")
		}
	}
	python.PyRun_SimpleString("pluginLoader.onLoad()")
}

func InitialSetup() {
	os.MkdirAll("plugins", 0777)
}

func cutPlugin(s []PluginInfo, i int) []PluginInfo {
	copy(s[i:], s[i+1:])
	return s[:len(s)-1]
}

func verCompare(v1, v2 string) bool {
	if strings.Contains(v1, "-") {
		v1 = strings.Split(v1, "-")[0]
	}
	if strings.Contains(v2, "-") {
		v2 = strings.Split(v2, "-")[0]
	}
	Log.Println("VerCompare", v1, v2)
	if f1, err := strconv.ParseFloat(v1, 64); err == nil {
		if f2, err := strconv.ParseFloat(v2, 64); err == nil {
			if f1 > f2 {
				return true
			}
		}
	}
	return false
}
