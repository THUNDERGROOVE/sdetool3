package main

import (
	"github.com/THUNDERGROOVE/SDETool3/util/log"
	"github.com/sbinet/go-python"
	"strings"
)

var LoggingMethods []python.PyMethodDef

func init() {
	LoggingMethods = []python.PyMethodDef{
		python.PyMethodDef{
			Name: "info",
			Meth: func(self, args *python.PyObject) *python.PyObject {
				size := python.PyTuple_GET_SIZE(args)
				o := []string{}
				for i := 0; i < size; i++ {
					item := python.PyTuple_GET_ITEM(args, i)
					v := python.PyString_AS_STRING(item)
					o = append(o, v)
				}
				log.Log.Println(strings.Join(o, " "))
				return nil
			},
			Flags: python.MethVarArgs,
			Doc:   "Write to the SDETool3 logger",
		},
	}
}
