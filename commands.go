package main

import (
	"os"
)

var (
	REPL      bool
	StdoutLog bool
)

func contains(sa []string, s string) (bool, int) {
	for k, v := range sa {
		if v == s {
			return true, k
		}
	}
	return false, 0
}

func cut(s []string, i int) []string {
	copy(s[i:], s[i+1:])
	return s[:len(s)-1]
}

func init() {
	if ok, i := contains(os.Args, "repl"); ok {
		REPL = true
		os.Args = cut(os.Args, i)
	}
	if ok, i := contains(os.Args, "log"); ok {
		StdoutLog = true
		os.Args = cut(os.Args, i)
	}
}
