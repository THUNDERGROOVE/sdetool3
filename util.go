package main

import (
	"bufio"
	"os"
	"strings"
)

func GenStrList(list []string) string {
	for k, v := range list {
		list[k] = "'" + v + "'"
	}
	var out string
	out += "["
	out += strings.Join(list, ",")
	out += "]"
	return out
}

func RawInput() string {
	in := bufio.NewReader(os.Stdin)
	line, _ := in.ReadString('\n')
	line = strings.Trim(line, "\n\r")
	return line
}
