all:
	@echo "Generating templates"
	@go-bindata -prefix templates templates/ 
	@echo "Building plugins"
	@cd dev-plugins/util && rm -rf ../../plugins/util.zip && zip ../../plugins/util.zip * && cd ../..
	@cd dev-plugins/SDEManager && rm -rf ../../plugins/SDEManager.zip && zip ../../plugins/SDEManager.zip * && cd ../..
	@echo "Building SDETool3"
	go build -ldflags "-X main.Commit `git rev-parse --short HEAD` -X main.Branch `git rev-parse --abbrev-ref HEAD` -X main.Version `git describe --tags`"

debug:
	go-bindata -debug -prefix templates templates/ 
	cd dev-plugins/util && rm -rf ../../plugins/util.zip && zip ../../plugins/util.zip * && cd ../..
	cd dev-plugins/SDEManager && rm -rf ../../plugins/SDEManager.zip && zip ../../plugins/SDEManager.zip * && cd ../..
	go build -ldflags "-X main.Commit `git rev-parse --short HEAD` -X main.Branch `git rev-parse --abbrev-ref HEAD` -X main.Version `git describe --tags`"
dist:
	@echo "Creating python2.7.zip"
	rm -rf python2.7.zip
	zip -r python2.7.zip /usr/lib/python2.7/* 

deploy:
	mkdir tmp
	cp SDETool3 tmp
	cp python2.7.zip tmp
	cd tmp
	zip SDETool3-`git rev-parse --abbrev-ref HEAD`-`git rev-parse --short HEAD`-linux-amd64.dist.zip SDETool3 python2.7.zip plugins/*
	cd .. 
	rm tmp -rf
