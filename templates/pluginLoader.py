from sys import path
path = []
path.append("python2.7.zip/usr/lib64/python2.7/")
import os
import os.path
path.append(os.getcwd())
import sys
import sqlite3
import cPickle as pickle

if not __SDETool3__:
	print("This must be run in SDEtool3")
	from os import exit
	exit(-1)

class Event():
	Type = ""
	Args = []
	def __init__(self, type, args=[]):
		self.Type = type
		self.Args = args


class PluginLoader():
	_plugins = []
	_pluginObjs = []
	_commands = {}
	_plainEvents = []
	SDE = None

	def __init__(self):
		pass

	def SetupPaths(self):
		for pl in os.listdir("plugins"):
			if pl.endswith(".zip"):
				self._plugins.append(pl.split(".zip")[0])
				sys.path.append("plugins/" + pl)

	def LoadPlugin(self, name, filename):
		if not os.path.exists("plugins/" + filename):
			print("Plugin does not exist")
			return
		exec("from " + name + " import Plugin")
		self._pluginObjs.append(Plugin(self))

	def onLoad(self):
		self.fireEvent("onLoad")

	def fireEvent(self, name, args=[]):
		for pl in self._pluginObjs:
			pl.handleEvent(Event(name, args))

	def RegisterCommandLineEvent(self, command, eventName, helpText="<No HelpText>"):
		self._commands[command] = (eventName, helpText)

	def RegisterEvent(self, eventName):
		self._plainEvents.append(eventName)

	def Plugin(self, pluginName):
		try:
			return self._pluginObjs[pluginName]
		except:
			return None

pluginLoader = PluginLoader()