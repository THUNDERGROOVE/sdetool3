package log

import (
	"fmt"
	"io"
	"log"
	"os"
)

var Log *log.Logger
var DoStdoutLog bool

func Init() {
	os.Remove("log.txt")
	var i io.Writer
	if file, err := os.OpenFile("log.txt", os.O_CREATE|os.O_WRONLY, 0777); err == nil {
		if DoStdoutLog {
			i = io.MultiWriter(file, os.Stdout)
		} else {
			i = file
		}
	} else if DoStdoutLog {
		fmt.Println("Error opening log file.  Defaulting to stdout")
		i = os.Stdout
	} else {
		fmt.Println("Not logging.  Couldn't open file and log flag not provided so not logging to Stdout.", err.Error())
	}
	Log = log.New(i, "[SDETool3]", log.Lshortfile)
}
