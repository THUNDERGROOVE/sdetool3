SDETool3
========
Kinda like SDETool and SDETool2 but extendable, and more pythony

Check docs for more information.

Building
========

A `go get bitbucket.org/THUNDERGROOVE/SDETool3` should suffice.

SDETool3 requires `go-python`

So far I've only been able to build it in linux.  Testing in Debian and CentOS.

I've had it almost compile in Windows but had issues linking the python2.7.lib, some weird incompatible library error?

Screenshots
===========
![Image1](http://bytebucket.org/THUNDERGROOVE/sdetool3/raw/master/imgs/1.png)
![Image2](http://bytebucket.org/THUNDERGROOVE/sdetool3/raw/master/imgs/2.png)
![Image3](http://bytebucket.org/THUNDERGROOVE/sdetool3/raw/master/imgs/3.png)