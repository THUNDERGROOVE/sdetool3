package main

import (
	"bitbucket.org/THUNDERGROOVE/SDETool3/util/log"
	"fmt"
	"github.com/sbinet/go-python"
	llog "log"
	"os"
	"strings"
	"text/template"
)

var (
	Templates map[string]*template.Template
	Log       *llog.Logger
)

func init() {

	Templates = make(map[string]*template.Template)

	for _, v := range AssetNames() {
		Templates[v] = template.Must(template.New(v).Parse(v))
	}
}

func main() {

	log.DoStdoutLog = StdoutLog
	log.Init()
	Log = log.Log

	Log.Println("SDETool3 started. ", Branch, Version, Commit)

	Log.Println("Starting Python engine..")
	if err := python.Initialize(); err != nil {
		Log.Println("  => Error: ", err.Error())
		return
	}
	python.PyRun_SimpleString("__SDETool3__ = True")
	Log.Println("Loading PluginLoader from memory")
	data, _ := Asset("pluginLoader.py")
	python.PyRun_SimpleString(string(data))

	LoadPlugins()

	global := python.PyImport_AddModule("__main__")
	PluginLoader := global.GenericGetAttr(python.PyString_FromString("pluginLoader"))
	if len(os.Args) != 1 {
		//command := os.Args[1]
		// HACK:  Fix this when CallMethod is implemented.
		python.PyRun_SimpleString(fmt.Sprintf("pluginLoader.fireEvent('cmd', %v)", GenStrList(os.Args[1:])))
	}

	if REPL {
		for {
			fmt.Print("\r                                 \r> ")
			com := RawInput()
			args := strings.Split(com, " ")[1:]
			com = strings.Split(com, " ")[:1][0]
			if com == "exit" {
				break
			}
			if com == "repl" {
				python.Py_Main([]string{""})
				continue
			}
			commands := PluginLoader.GetAttr(python.PyString_FromString("_commands"))
			if ok, err := python.PyDict_Contains(commands,
				python.PyString_FromString(com)); err != nil {
				Log.Println("\nError:", err.Error())
			} else if ok {
				eventTup := python.PyDict_GetItem(commands, python.PyString_FromString(com))
				event := python.PyTuple_GET_ITEM(eventTup, 0)
				argss := GenStrList(args)
				// HACK: Fix this when CallMethod is implemented
				python.PyRun_SimpleString(
					"pluginLoader.fireEvent('" + python.PyString_AS_STRING(event) + "', " + argss + ")")
			} else {
				Log.Println("\nCommand does not exist")
			}
		}
	}
}
