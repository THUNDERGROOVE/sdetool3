package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"strings"
	"os"
	"time"
	"io/ioutil"
	"path"
	"path/filepath"
)

func bindata_read(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindata_file_info struct {
	name string
	size int64
	mode os.FileMode
	modTime time.Time
}

func (fi bindata_file_info) Name() string {
	return fi.name
}
func (fi bindata_file_info) Size() int64 {
	return fi.size
}
func (fi bindata_file_info) Mode() os.FileMode {
	return fi.mode
}
func (fi bindata_file_info) ModTime() time.Time {
	return fi.modTime
}
func (fi bindata_file_info) IsDir() bool {
	return false
}
func (fi bindata_file_info) Sys() interface{} {
	return nil
}

var _pluginloader_py = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\x6c\x53\x4f\x6f\xd4\x3e\x10\x3d\x27\x9f\xc2\xf2\x29\xd1\xaf\xbf\x2c\x50\x04\x52\x45\x91\x10\x54\xe2\x50\x95\x8a\xee\x6d\xb5\x8a\xb2\xc9\x64\x63\x70\x6c\x63\x7b\xe9\x2e\x88\xef\xce\xf8\x5f\x92\x6d\xb9\xec\x4e\xec\x37\x6f\x66\xde\x3c\xf7\x5a\x8e\xc4\x9c\x0c\x61\xa3\x92\xda\x12\xd5\xd8\x21\x77\x3f\xe4\x9a\x6c\xb6\x3e\xaa\x1a\xa5\x40\x74\x05\x55\x27\x3b\x48\xf1\xaa\x7a\x5b\xfd\x62\x6a\x75\x30\x7a\xc5\xd9\xee\xcd\xeb\xd5\x74\xbe\xa2\x65\x1e\x79\xa4\x99\xa3\x6a\x22\x4d\x54\x78\xb6\x07\xdb\x3e\x76\x45\x39\x65\x60\x13\x53\xf8\x83\x33\x0b\x97\xe9\xb3\xbd\x67\xed\x77\x0e\xa4\x31\x44\xf9\x28\xcf\x59\x4f\x84\xb4\xa4\xae\x1f\x3e\xdd\xac\xa5\xe4\x97\x75\x7d\x95\x67\x4a\x33\x61\x0b\xba\x1e\x98\x21\xe3\xc1\x58\xb2\x03\xa2\x0f\x82\x30\x41\x10\x67\x1d\x0e\x3b\xcc\xfc\xcc\x72\x1a\x19\x8e\xcc\xe6\x99\xfb\x2d\xfe\x7f\x59\xe6\x79\xcb\x1b\x63\xc8\xcd\x4f\x40\xae\x12\x59\xd7\x27\x05\xa8\x06\xa5\x79\xf6\x41\xef\x4d\x10\x26\xeb\xa0\xc7\xf2\x4c\x30\x5b\xd7\x85\x01\xde\x5f\x10\x8b\xc0\x0b\xd2\x20\xe6\x7a\xb3\x75\x99\x99\x3b\xaf\x62\xbe\xbb\x4d\x47\x91\xc7\x41\xf3\x54\xf0\x9e\x1f\xf6\x4c\xdc\xca\xa6\x03\xed\xeb\xd6\xca\x9f\xa4\x82\xf1\xf3\xcb\xee\xdb\x74\xd2\xca\x71\x6c\x44\xe7\xbe\x7f\xff\xf1\x88\x86\x09\xdf\x78\x82\xe0\xd8\x18\xdd\x49\x81\x9a\x3d\x6f\xd9\xf7\xa8\xb0\x78\xbc\x7c\x00\x7b\x50\xf7\xb8\x27\x33\x5f\xf7\x52\x13\xc5\x9d\x84\xb8\x34\xce\x8c\xed\x98\x46\x2b\x84\xd6\xa8\x87\x64\xb8\x0d\xc5\x2b\x5c\xac\x79\x64\x76\x28\xa8\xf3\x47\xbc\x0a\xf3\xa6\x51\xd2\xfe\x11\x6d\x14\xee\x38\x41\x37\x2f\xb6\x65\x40\x9f\x82\x5b\x66\xcf\x85\xc4\x15\x25\xff\x61\x8d\x32\x36\xea\x54\x0a\x7a\x45\xe9\x45\x33\xa2\xf4\x3d\xe3\xe0\x22\x5f\x3a\x5a\x24\xda\xaf\xc2\xfd\x1a\x6b\xce\x09\xcf\xf0\xc9\x3d\x81\x97\x74\x12\x8c\x27\xf0\x89\xd4\xb7\xa7\x51\x1f\x2d\x30\x82\x23\xb4\x05\xf5\x3e\x72\x3c\x8e\x03\xff\x68\x72\x54\xa0\xf0\x39\xcb\xf1\xdd\xea\xd2\x60\x8b\xee\xcb\x34\x95\xf4\xdb\x9f\xa5\xf7\xb9\x3d\xd3\x10\xbc\x48\xc3\x3d\x4d\xf0\xf9\x66\xa9\xc1\xd2\x7e\xf3\xee\x9e\xb6\x11\x06\xe6\xd5\x80\xf6\xe1\x91\x25\xfc\xce\x2c\x53\x5f\x5f\x61\x8f\x12\x80\xfe\x18\xec\x76\xcb\xc4\x59\xdd\xe8\xc2\x0b\x02\xee\xf0\xce\xe7\x0f\xc0\xd5\x1a\x8e\xf6\x9a\xbe\xbb\x93\xe4\x73\xfc\x7a\x4f\xe7\xb9\x26\xf3\x6e\x62\xb0\x45\xa3\x16\xff\xa0\x78\xda\xc5\xb2\xf4\x04\x5f\xf0\x2e\x1e\x41\x12\x7b\x86\x45\xae\x33\xef\x04\x55\x26\x16\xab\x4f\x57\xf3\xb2\x9f\x29\xb7\x99\xe1\x5b\xef\x84\x16\x94\x5d\x26\x84\xc7\xa6\x16\xaf\x19\x07\x3b\x7f\xdc\x7f\x03\x00\x00\xff\xff\x77\x68\x5e\xc4\x75\x05\x00\x00")

func pluginloader_py_bytes() ([]byte, error) {
	return bindata_read(
		_pluginloader_py,
		"pluginLoader.py",
	)
}

func pluginloader_py() (*asset, error) {
	bytes, err := pluginloader_py_bytes()
	if err != nil {
		return nil, err
	}

	info := bindata_file_info{name: "pluginLoader.py", size: 1397, mode: os.FileMode(420), modTime: time.Unix(1430363946, 0)}
	a := &asset{bytes: bytes, info:  info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if (err != nil) {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"pluginLoader.py": pluginloader_py,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for name := range node.Children {
		rv = append(rv, name)
	}
	return rv, nil
}

type _bintree_t struct {
	Func func() (*asset, error)
	Children map[string]*_bintree_t
}
var _bintree = &_bintree_t{nil, map[string]*_bintree_t{
	"pluginLoader.py": &_bintree_t{pluginloader_py, map[string]*_bintree_t{
	}},
}}

// Restore an asset under the given directory
func RestoreAsset(dir, name string) error {
        data, err := Asset(name)
        if err != nil {
                return err
        }
        info, err := AssetInfo(name)
        if err != nil {
                return err
        }
        err = os.MkdirAll(_filePath(dir, path.Dir(name)), os.FileMode(0755))
        if err != nil {
                return err
        }
        err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
        if err != nil {
                return err
        }
        err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
        if err != nil {
                return err
        }
        return nil
}

// Restore assets under the given directory recursively
func RestoreAssets(dir, name string) error {
        children, err := AssetDir(name)
        if err != nil { // File
                return RestoreAsset(dir, name)
        } else { // Dir
                for _, child := range children {
                        err = RestoreAssets(dir, path.Join(name, child))
                        if err != nil {
                                return err
                        }
                }
        }
        return nil
}

func _filePath(dir, name string) string {
        cannonicalName := strings.Replace(name, "\\", "/", -1)
        return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}

